import '../styles/styles.scss'

function Showcase({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default Showcase
