import Head from 'next/head'
import Image from 'next/image'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Employee CRUD App</title>
        <meta
          name="description"
          content="Showcase for React Hooks and NextJS, with Hasura backend"
        />
        <link
          rel="icon"
          href="/favicon.ico"
        />
      </Head>

      <main>

      </main>

      <footer>

      </footer>
    </div>
  )
}
